#!/bin/bash

# This should not be run on a client
# Only use this to modify the CDStack template

disable_functions=`cat disable_functions.txt | sort | uniq | awk '{printf "%s%s",sep,$0; sep=","} END{print ""}'`

find . -type f ! -name php_modify_vars.sh -print0 | xargs -0 sed -i -E "s/(\;)?(\ )?upload_max_filesize[ ]?=.*/upload_max_filesize = 128M/g"
find . -type f ! -name php_modify_vars.sh -print0 | xargs -0 sed -i -E "s/(\;)?(\ )?post_max_size[ ]?=.*/post_max_size = 128M/g"
find . -type f ! -name php_modify_vars.sh -print0 | xargs -0 sed -i -E "s/(\;)?(\ )?memory_limit[ ]?=.*/memory_limit = 512M/g"
find . -type f ! -name php_modify_vars.sh -print0 | xargs -0 sed -i -E "s/(\;)?(\ )?max_input_vars[ ]?=.*/max_input_vars = 10000/g"
find . -type f ! -name php_modify_vars.sh -print0 | xargs -0 sed -i -E "s/(\;)?(\ )?session.use_strict_mode[ ]?=.*/session.use_strict_mode = On/g"
find . -type f ! -name php_modify_vars.sh -print0 | xargs -0 sed -i -E "s/(\;)?(\ )?session.cookie_secure[ ]?=.*/session.cookie_secure = On/g"
find . -type f ! -name php_modify_vars.sh -print0 | xargs -0 sed -i -E "s/(\;)?(\ )?session.cookie_httponly[ ]?=.*/session.cookie_httponly = On/g"
find . -type f ! -name php_modify_vars.sh -print0 | xargs -0 sed -i -E "s/(\;)?(\ )?session.cookie_lifetime[ ]?=.*/session.cookie_lifetime = 0/g"
find . -type f ! -name php_modify_vars.sh -print0 | xargs -0 sed -i -E "s/(\;)?(\ )?apc.enabled[ ]?=.*/apc.enabled = 1/g"
find . -type f ! -name php_modify_vars.sh -print0 | xargs -0 sed -i -E "s/(\;)?(\ )?apc.enable_cli[ ]?=.*/apc.enable_cli = 1/g"
find . -type f ! -name php_modify_vars.sh -print0 | xargs -0 sed -i -E "s/(\;)?(\ )?opcache.enable[ ]?=.*/opcache.enable = 1/g"
find . -type f ! -name php_modify_vars.sh -print0 | xargs -0 sed -i -E "s/(\;)?(\ )?opcache.enable_cli[ ]?=.*/opcache.enable_cli = 1/g"
find . -type f ! -name php_modify_vars.sh -print0 | xargs -0 sed -i -E "s/(\;)?(\ )?expose_php[ ]?=.*/expose_php = Off/g"
#find . -type f ! -name php_modify_vars.sh -print0 | xargs -0 sed -i -E "s/(\;)?(\ )?disable_functions[ ]?=.*/disable_functions = ${disable_functions}/g"
find . -type f ! -name php_modify_vars.sh -print0 | xargs -0 sed -i -E "s/(\;)?(\ )?disable_functions[ ]?=.*/disable_functions =/g"
find . -type f ! -name php_modify_vars.sh -print0 | xargs -0 sed -i -E "s/(\;)?(\ )?error_reporting[ ]?=.*/error_reporting = E_ALL \& \~E_DEPRECATED \& \~E_STRICT \& \~E_NOTICE/g"
find . -type f ! -name php_modify_vars.sh -print0 | xargs -0 sed -i -E "s/(\;)?(\ )?catch_workers_output[ ]?=.*/catch_workers_output = yes/g"

# Skip for CLI
find . -type f ! -name php_modify_vars.sh ! -path '*cli*' -print0 | xargs -0 sed -i -E 's/(\;)*(\ )*max_input_time =.*/max_input_time = 300/g'
find . -type f ! -name php_modify_vars.sh ! -path '*cli*' -print0 | xargs -0 sed -i -E 's/(\;)?(\ )?max_execution_time =.*/max_execution_time = 300/g'
