#!/bin/sh

export DEBIAN_FRONTEND=noninteractive
export APT_LISTCHANGES_FRONTEND=none
export OSSEC_ACTION_CONFIRMED=y

# Mark etc as safe directory for git
git config --global --add safe.directory /etc

# Secure built-in admin user
passwd -d admin
passwd -l admin

# Modify system environment
chmod 644 /etc/krb5.conf
chmod 644 /etc/network/interfaces
chmod 644 /etc/network/interfaces.new
chmod 644 /etc/network/interfaces.d/*
chmod -R +rx /etc/network/if-up.d/*
chmod -R +rx /etc/network/if-pre-up.d/*
chmod -R +rx /etc/network/if-post-down.d/*
chmod -R +rx /etc/network/if-down.d/*
chmod +rx /etc/environment
chmod +x /etc/profile.d/*
chmod -R g+r,o+r /etc/hosts
chmod -R g+r,o+r /etc/nsswitch.conf
chmod -R g+r,o+r /etc/resolv.conf
chmod -R g+r,o+r /etc/ntp.conf
chmod -R g+r,o+r /etc/systemd/timesyncd.conf
chmod -R g+r,o+r /etc/chrony/chrony.conf

# Set correct permissions on service defaults
chmod -R +r /etc/default/*

# Update grub and kernel params
update-grub2

# Update CMDB client
cp /opt/cdstack-cmdb-deploy/files/usr/sbin/cmdb-deploy /usr/sbin/cmdb-deploy
chown root:root /usr/sbin/cmdb-deploy
chown root:root /usr/sbin/cmdb-deploy-dev
chmod 770 /usr/sbin/cmdb-deploy
chmod 770 /usr/sbin/cmdb-deploy-dev

# Set cron owner
chown root:root /etc/cron.d/codeorange-cmdb
chmod 0644 /etc/cron.d/codeorange-cmdb

# APT: Set owner and sync packages if enabled
chown -R _apt:root /etc/apt
chmod -R +r /etc/apt/apt.conf.d
chmod +r /etc/apt/sources.list
chmod -R +r /etc/apt/sources.list.d
chmod -R +r /etc/apt/preferences.d
chmod +r /etc/apt/trusted.gpg
chmod -R +r /etc/apt/trusted.gpg.d
{% if cmdb_host.pkg_sync_enable == 1 %}
# Sync packages from image
apt-get update --allow-releaseinfo-change
apt-get install --fix-broken --assume-yes
apt-get purge --yes --force-yes $(cat /etc/cmdb/apt_packages_purge.txt)
apt-get install --yes -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" --force-yes --no-upgrade --ignore-missing $(cat /etc/cmdb/apt_packages.txt)
{% endif %}

# Generate DH
if [ -f "/etc/nginx/dhparam.pem" ]
then
    echo "DH Param found."
else
    echo "DH Param not found, generating..."
    openssl dhparam -out /etc/nginx/dhparam.pem 4096
fi

# Configure PHP
chmod -R g+r,o+r /etc/php

# Set default PHP version
update-alternatives --set php /usr/bin/php7.4
update-alternatives --set php-cgi /usr/bin/php-cgi7.4
update-alternatives --set phar /usr/bin/phar7.4
update-alternatives --set phar.phar /usr/bin/phar.phar7.4
update-alternatives --set phpize /usr/bin/phpize7.4
update-alternatives --set php-config /usr/bin/php-config7.4

# Download 3rd party PHP extensions
# Ioncube loader
chmod +x /usr/local/ioncube/ioncube_download_and_install.sh
sh /usr/local/ioncube/ioncube_download_and_install.sh
# SourceGuardian loader
chmod +x /usr/local/sourceguardian/sourceguardian_download_and_install.sh
sh /usr/local/sourceguardian/sourceguardian_download_and_install.sh

# Reload Service Config
systemctl daemon-reload

# Disable cloud-init
systemctl disable cloud-init
systemctl mask cloud-init

# WAZUH/OSSEC - security monitoring solution for threat detection, integrity monitoring, incident response and compliance
{% if monitoring_wazuh_agent_key %}
/var/ossec/bin/manage_agents -i "{{ monitoring_wazuh_agent_key }}"
chown -R ossec:ossec /var/ossec/
systemctl enable wazuh-agent.service
{% endif %}

# PHPWorker services
systemctl enable nginx
systemctl enable apache2

a2enmod headers
a2enmod ssl
a2enmod http2
a2enmod rpaf
a2dismod evasive
a2dismod defensible
a2dismod security2
a2dismod shib
a2disconf shib
a2enmod remoteip
a2enconf httpoxy
a2enconf tls_security
a2ensite 000-default
a2dissite default-ssl

systemctl enable php5.6-fpm
systemctl enable php7.0-fpm
systemctl enable php7.1-fpm
systemctl enable php7.2-fpm
systemctl enable php7.3-fpm
systemctl enable php7.4-fpm

cp /etc/code-orange/whstack-backend-client.conf /opt/whstack-backend-client/.env
/bin/bash /opt/whstack-backend-client/install.sh
/bin/bash /opt/whstack-backend-client/whstack_client.sh

# Basic System Services
chown -R mail:mail /etc/dma/
chmod +r /etc/postfix/master.cf
chmod +r /etc/postfix/main.cf
chmod 644 /etc/mailname
chmod 644 /etc/aliases
systemctl enable zramswap
systemctl enable haveged
systemctl enable sshd
systemctl enable ntp || systemctl enable chrony
systemctl enable lldpd
systemctl enable snmpd

# System Monitoring
chown -R nagios:nagios /etc/icinga2
chown -R nagios:nagios /var/lib/icinga2
systemctl enable icinga2
systemctl enable salt-minion
systemctl enable smartd
systemctl enable watchdog
systemctl disable corosync
systemctl disable pacemaker
sensors-detect --auto

# Filesystem Maintenance
systemctl enable fstrim.timer

# VM-Guest Tools
systemctl enable qemu-guest-agent

# Security and Firewall
systemctl unmask shorewall
systemctl enable shorewall
systemctl unmask shorewall6
systemctl enable shorewall6

{% if fail2ban_apps %}sh /usr/local/sbin/fail2ban_touch_logs.sh{% endif %}
systemctl unmask fail2ban
systemctl enable fail2ban
systemctl enable crowdsec.service
systemctl enable crowdsec-firewall-bouncer.service


etckeeper commit "cmdb-deploy-specific-post" || true
systemctl enable etckeeper.service etckeeper.timer
